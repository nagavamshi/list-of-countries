//
//  Countries.swift
//  Countries
//
//  Created by Neelesh on 19/08/20.
//  Copyright © 2020 Neelesh. All rights reserved.
//

import Foundation

struct Countries: Codable {
    var success: Bool!
    var message: String?
    var result: [Country]!
}

struct Country: Codable {
    var id: Int?
    var sortname: String?
    var name: String?
}

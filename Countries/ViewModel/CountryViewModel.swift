//
//  CountryViewModel.swift
//  Countries
//
//  Created by Neelesh on 15/09/20.
//  Copyright © 2020 Neelesh. All rights reserved.
//

import Foundation

class CountryViewModel {
    
    var countries = Countries()
    private var service = Service()
    private var serviceURL = "http://3.6.254.61:3002/common/listCountries"
    
    var receivedData: (() -> ())?
    
    func getCountriesData() {
        
        service.getRequest(url: serviceURL, decodingType: Countries.self) { [weak self] (result) in
            
            switch result {
                case .success(let data):
                    self?.countries = data
                    self?.receivedData?()
                
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }    
}
